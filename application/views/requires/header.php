  <div class="wrapper">

      <header class="main-header">

          <!-- LOGOTIPO -->
          <a href="" class="logo" style="background: #080b0c;">
           <img src="<?= base_url()  ?>public/img/logoanvi-blanco.png" width="180px" height="41px">
          </a>


 
          <!-- BARRA SUPERIOR -->
          <nav class="navbar navbar-static-top" role="navigation">

          <!-- BOTON PARA DESPLEGAR MENU LATERAL -->
             <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button" style="background:#3fc7c3; padding: 5px;">
            </a>

            <!-- USUARIO A LA DERECHA DE LA BARRA SUPERIOR (DESPLEGA UN PEQUEÑO CUADRO PARA SALIR) -->
            <div class="navbar-custom-menu" > 
              <!-- esta clase CUSTOM-MENU sirve para que el recuadro pueda estar fuera de la barra superior -->
              <ul class="nav navbar-nav">
           
                  <li class="dropdown user-menu">
                    <img src="<?= base_url()  ?>public/img/usuario.png" class="user-image" alt="User Image">
                    <a href="#" class="usersalir" data-toggle="dropdown">
                      <span><?= $this->session->userdata('user');  ?></span>
                    </a>
                        <ul class="dropdown-menu">
                          
                          <li class="user-header">
                            <img src="<?= base_url()  ?>public/img/usuario.png" class="img-circle" alt="User Image">
                            <p style="color: black;">

                              Estás logeado como: <br><?= ucfirst($this->session->userdata("tipo"));  ?>
                            </p>
                          </li>
                          <li class="user-footer"  style="background: #3fc7c3">
                            <div class="pull-right">
                              <a href="<?= base_url() ?>panel/salir" class="btn btn-danger">Cerrar Sesion</a>
                            </div>
                          </li>
                        </ul>
                  </li>
                
              </ul>

            </div>

          </nav>
        <script src="<?= base_url()  ?>public/jquery/jquery-3.3.1.min.js"></script>

      </header>
