    <div class="modal fade" id="nuevacategoria" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">

                <form action="<?= base_url() ?>panel/actualizarUsuario" id="loginForm" method="POST" class="form-group">  
                    <div class="container">
               
              
                        <label> Usuario </label><br>
                        <input type="text" value="<?= $this->session->userdata("user") ?>" class="form-control" name="usuario" id="usuario" /> 

                        <label> Clave </label><br>
                        <input type="password" value="<?= $this->session->userdata("clave") ?>" class="form-control" name="clave" id="clave"/> 
                        <br>

                        <button type="submit" class="form-control" onclick="guardado()">Actualizar usuario</button>
              


                    </div>
                </form>
        </div>
      </div>
    </div>


 <footer class="main-footer color-foot">
      <div class="pull-right hidden-xs">
          <b>Version</b> 0.3
      </div>
        <strong>Derechos Reservados | ANVICOR'S, City Market &copy</strong>
</footer>  

	<script src="<?= base_url()  ?>public/jquery/jquery-3.3.1.min.js"></script>
	<script src="<?= base_url()  ?>public/Bootstrap/js/bootstrap.min.js"></script>
    <script src="<?= base_url()  ?>public/adminLTEjs/app.min.js"></script>

<!-- DATATABLES de JQUERY -->

     <script type="text/javascript" src="<?= base_url()  ?>public/datatable/tables/js/jquery.dataTables.min.js"></script>
     <script type="text/javascript" src="<?= base_url()  ?>public/datatable/buttons/js/dataTables.buttons.min.js"></script>
     <script type="text/javascript" src="<?= base_url()  ?>public/datatable/export/flash-button.js"></script>
     <script type="text/javascript" src="<?= base_url()  ?>public/datatable/export/jszip.js"></script>
     <script type="text/javascript" src="<?= base_url()  ?>public/datatable/export/pdf.js"></script>
     <script type="text/javascript" src="<?= base_url()  ?>public/datatable/export/vfs_fonts.js"></script>
     <script type="text/javascript" src="<?= base_url()  ?>public/datatable/export/html-buttons.js"></script>
     <script type="text/javascript" src="<?= base_url()  ?>public/datatable/export/vfs_fonts.js"></script>
     <script type="text/javascript" src="<?= base_url()  ?>public/datatable/idioma.js"></script>


<!-- ALERTIFY -->
    <script type="text/javascript" src="<?= base_url()  ?>public/alertifyjs/alertify.js"></script>


<!-- swet -->

     <script type="text/javascript" src="<?= base_url()  ?>public/Bootstrap/js/sweetalert.min.js"></script>

<!-- Scripts -->

    <script type="text/javascript"> 

             var url = "<?= base_url() ?>";

             function index(){
                    $('#spanError1').hide();
                    $('#spanError2').hide();
            }
   
            index();
       
    </script>

     <script src="<?= base_url()  ?>public/js/general.js"></script>

     <?php if($this->uri->segment(3)=='articulos' || $this->uri->segment(3)=='consultar') {?> 
        <script src="<?= base_url() ?>public/js/combos.js"></script>
        <script src="<?= base_url()  ?>public/js/articulos.js"></script>
     <?php }?>

     <?php if($this->uri->segment(3)=='categorias')  {?> 
         <script src="<?= base_url()  ?>public/js/categorias.js"></script>
     <?php }?>  

     <?php if($this->uri->segment(3)=='proveedores')  {?> 
         <script src="<?= base_url()  ?>public/js/proveedores.js"></script>
     <?php }?> 



 </body>
 </html>