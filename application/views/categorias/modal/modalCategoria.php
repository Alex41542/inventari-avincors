<!-- MODAL PARA REGISTRAR UNA NUEVA CATEGORIA -->

    <div class="modal fade" id="modal_categoria" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">

          <div class="modal-header bg-header-modal">
            <h4 class="modal-title" id="myModalLabel"><i class="fa fa-folder""></i> Nueva Categoria</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" onclick="limpiar()">&times;</span></button> 
          </div>


          <div class="modal-body bg-body-modal">
            <form id="form">
               <input type="hidden" value="" name="id_categoria" id="id_categoria">
               <b><label for="c_nombre">Nombre</label></b>
                <input type="text" name="c_nombre" placeholder="Nombre" class="form-control input-sm limpiar" id="c_nombre"/>
                <span id="spanError1">Debes introducir el nombre de tu categoria <br></span>

               <b> <label for="c_descripcion">Descripcion</label></b>
                <textarea name="c_descripcion" placeholder="Descripcion breve" class="form-control input-sm limpiar" rows="10" cols="50" id="c_descripcion"></textarea>
                 <span id="spanError2">Debes introducir la descripción de tu categoria</span>
             </form>
          </div>

          <div class="modal-footer bg-footer-modal">
            <button type="button" class="btn btcerrar" data-dismiss="modal" onclick="limpiar()">Cerrar</button>
            <button class="btn btcrear" onclick="save()" id="createcategoria">Crear</button>
          </div>
        </div>
      </div>
    </div>
         
