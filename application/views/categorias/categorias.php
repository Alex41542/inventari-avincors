<body class="skin-green" onload="loadtable()">

  <!-- Content-Wrapper para el contenedor que tendra el contenido -->
      <div class="content-wrapper">
        
        <!-- contenido -->
        <section class="content">
          <!-- Una fila -->
            <div class="row"">
              <!-- 12 columnas -->
              <div class="col-md-12">
                  <div class="box">
                    <!-- ENCABEZADO DE LA CAJA  -->
                        <div class="box-header bg1">
                          <button class="btn bt1 pull-right" data-toggle="modal" onclick="add_categoria()"><i class="fa fa-plus-circle "></i> Agregar</button>
                              <h3><i class="fa fa-tags"></i> Categorías </h3>
                        
                        </div>
                        
                        <!-- Centro de la caja  -->
                        <div class="panel-body table-responsive tableresp">
                            <div class="container">
                                      <hr>
                                   
                            <!-- TABLA -->
                                
                                    <table id="tb_categoria" class="table table-hover table-bordered table-striped" cellspacing="0" width="100%">
                                      <thead class="thead-dark">
                                          <tr>
                                              <th>NOMBRE</th>
                                              <th>DESCRIPCION</th>
                                              <th>FECHA AGREGADO</th>
                                              <th>OPCIONES</th>
                                           </tr>

                                      </thead>
                                       <tbody id="tbody">

                                       </tbody>
                                     </table>
                            </div>
                      
                       </div>
                  </div>
              </div>
          </div>
      </section>

    </div>

<!-- FIN CONTENIDO DE PAGINA  -->

<?php require "modal/modalCategoria.php" ?>
