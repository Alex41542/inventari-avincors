<!-- MODAL PARA REGISTRAR UN NUEVO PROVEEDOR -->

    <div class="modal fade" id="modal_proveedor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">

          <div class="modal-header bg-header-modal">
            <h4 class="modal-title" id="myModalLabel"><i class="fa fa-truck"></i> Nuevo Proveedor</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" onclick="limpiar()">&times;</span></button> 
          </div>


          <div class="modal-body bg-body-modal">
            <form id="form_proveedor">
                <input type="hidden" value="" name="id_proveedor" id="id_proveedor">
               <b><label for="p_nombre">Nombre</label></b>
                <input type="text" name="p_nombre" placeholder="Nombre" class="form-control input-sm limpiar" id="p_nombre"/>
                

                <b><label for="p_numero1">Número Celular</label></b>
                <input type="text" name="p_numero1" placeholder="Numero celular (sin guiones)" class="form-control input-sm limpiar" id="p_numero1"/>

                <b><label for="p_numero2">Número Oficina</label></b>
                <input type="text" name="p_numero2" placeholder="Numero oficina (sin guiones)" class="form-control input-sm limpiar" id="p_numero2"/> 

                 <b><label for="p_correo">Correo Electronico</label></b>
                <input type="text" name="p_correo" placeholder="Correo" class="form-control input-sm limpiar" id="p_correo"/>
                 <div id="spanError2"><div class="errors"></div></div>
            </form>
          </div>


          <div class="modal-footer bg-footer-modal">
            <button type="button" class="btn btcerrar" data-dismiss="modal" onclick="limpiar()">Cerrar</button>
            <button class="btn btcrear" onclick="save()" id="createproveedor">Crear</button>
          </div>
        </div>
      </div>
    </div>
         
<!--  FIN MODAL PARA REGISTRAR UN NUEVO PROVEEDOR -->
 
<!-- MODAL PARA EDITAR PROVEEDOR -->

    <div class="modal fade" id="editarproveedor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">

          <div class="modal-header bg-header-modal">
            <h4 class="modal-title" id="myModalLabel"><i class="fa fa-edit"></i> Modificar Proveedor</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" onclick="limpiar()">&times;</span></button> 
          </div>


          <div class="modal-body bg-body-modal">
            
               <b><label for="pnombre">Nombre</label></b>
                <input type="text" name="pnombre" placeholder="Nombre" class="form-control input-sm limpiar"/>

                  <b><label for="pnumero">Número</label></b>
                <input type="text" name="pnumero" placeholder="Introduce el número sin guiones y puntos" class="form-control input-sm limpiar"/>

                 <b><label for="pcorreo">Correo Electronico</label></b>
                <input type="text" name="pcorreo" placeholder="Correo" class="form-control input-sm limpiar"/>
                 
          </div>


          <div class="modal-footer bg-footer-modal">
            <button type="button" class="btn btcerrar" data-dismiss="modal" onclick="limpiar()">Cerrar</button>
            <button class="btn btn-warning">Actualizar</button>
          </div>
        </div>
      </div>
    </div>
         
<!--  FIN MODAL PARA EDITAR PROVEEDOR -->

