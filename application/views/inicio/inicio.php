<!-- full calendar -->

    
    <link href='<?= base_url()  ?>public/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
    <link href='<?= base_url()  ?>public/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
    <script src='<?= base_url()  ?>public/fullcalendar/lib/moment.min.js'></script>
    <script src='<?= base_url()  ?>public/fullcalendar/lib/jquery.min.js'></script>
    <script src='<?= base_url()  ?>public/fullcalendar/fullcalendar.min.js'></script>
    
    <script>

  $(document).ready(function() {

    $('#calendar').fullCalendar({
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,basicWeek,basicDay'
      },
      defaultDate: '2018-08-12',
      navLinks: true, // can click day/week names to navigate views
      editable: true,
      eventLimit: true, // allow "more" link when too many events
      events: [
        {
          title: 'Tesis',
          start: '2018-08-17'
        },
        {
          title: 'Bancario',
          start: '2018-08-19'
        },
   
      ]
    });

  });

</script>
    <style type="text/css">
        
#calendar {
    max-width: 1050px;
    margin: 0 auto;
  }

    </style>


<body class="skin-green">
  <!-- Content-Wrapper para el contenedor que tendra el contenido -->
      <div class="content-wrapper">
        
        <!-- contenido -->
        <section class="content">
          <!-- Una fila -->
            <div class="row"">
              <!-- 12 columnas -->
              <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3><?= $cantArticulos  ?></h3>

                <p>Articulos</p>
            </div>
            <div class="icon">
                <i class="fa fa-tablet"></i>
            </div>
            <a href="<?= base_url() ?>panel/page/articulos" class="small-box-footer">Mas información <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-green">
            <div class="inner">
                <h3><?= $cantCategorias  ?></h3>

                <p>Categorias</p>
            </div>
            <div class="icon">
                <i class="fa fa-tags"></i>
            </div>
            <a href="<?= base_url() ?>panel/page/categorias" class="small-box-footer">Mas información<i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-yellow">
            <div class="inner">
                <h3><?= $cantProveedores  ?></h3>

                <p>Proveedores</p>
            </div>
            <div class="icon">
                <i class="fa fa-truck"></i>
            </div>
            <a href="<?= base_url() ?>panel/page/proveedores" class="small-box-footer">Mas información <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-red">
            <div class="inner">
                <h3>1</h3>

                <p>Usuario</p>
            </div>
            <div class="icon">
                <i class="fa fa-user-plus"></i>
            </div>
            <a onclick="dialog()" class="small-box-footer"> Editar Usuario <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->

    <div id="calendar" style="background: #F5E6A8; border-radius: 10px;"></div>
</div>
<!-- /.row -->


       </section>

    </div>

<style type="text/css">
    .fc-next-button, .fc-prev-button{
        background: #339DEE;
    }

    .fc-header-toolbar{
        background: #3B3A3A;
        color: white;
    }

    .fc-month-button, .fc-basicWeek-button, .fc-basicDay-button{
        background: #339DEE;
    }
</style>
<!-- FIN CONTENIDO DE PAGINA -->

 <div class="modal fade" id="nuevacategoria" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">

                <form action="<?= base_url() ?>panel/actualizarUsuario" id="loginForm" method="POST" class="form-group">  
                    <div class="container">
               
              
                        <label> Usuario </label><br>
                        <input type="text" value="<?= $this->session->userdata("user") ?>" class="form-control" name="usuario" id="usuario" /> 

                        <label> Clave </label><br>
                        <input type="password" value="<?= $this->session->userdata("clave") ?>" class="form-control" name="clave" id="clave"/> 
                        <br>

                        <button type="submit" class="form-control" onclick="guardado()">Actualizar usuario</button>
              


                    </div>
                </form>
        </div>
      </div>
    </div>


 <footer class="main-footer color-foot">
      <div class="pull-right hidden-xs">
          <b>Version</b> 0.3
      </div>
        <strong>Derechos Reservados | ANVICOR'S, City Market &copy</strong>
</footer>  


    <script src="<?= base_url()  ?>public/Bootstrap/js/bootstrap.min.js"></script>
    <script src="<?= base_url()  ?>public/adminLTEjs/app.min.js"></script>

<!-- DATATABLES de JQUERY -->

     <script type="text/javascript" src="<?= base_url()  ?>public/datatable/tables/js/jquery.dataTables.min.js"></script>
     <script type="text/javascript" src="<?= base_url()  ?>public/datatable/buttons/js/dataTables.buttons.min.js"></script>
     <script type="text/javascript" src="<?= base_url()  ?>public/datatable/export/flash-button.js"></script>
     <script type="text/javascript" src="<?= base_url()  ?>public/datatable/export/jszip.js"></script>
     <script type="text/javascript" src="<?= base_url()  ?>public/datatable/export/pdf.js"></script>
     <script type="text/javascript" src="<?= base_url()  ?>public/datatable/export/vfs_fonts.js"></script>
     <script type="text/javascript" src="<?= base_url()  ?>public/datatable/export/html-buttons.js"></script>
     <script type="text/javascript" src="<?= base_url()  ?>public/datatable/export/vfs_fonts.js"></script>
     <script type="text/javascript" src="<?= base_url()  ?>public/datatable/idioma.js"></script>


<!-- ALERTIFY -->
    <script type="text/javascript" src="<?= base_url()  ?>public/alertifyjs/alertify.js"></script>


<!-- swet -->

     <script type="text/javascript" src="<?= base_url()  ?>public/Bootstrap/js/sweetalert.min.js"></script>

<!-- Scripts -->

    <script type="text/javascript"> 

             var url = "<?= base_url() ?>";

             function index(){
                    $('#spanError1').hide();
                    $('#spanError2').hide();
            }
   
            index();
       
    </script>

     <script src="<?= base_url()  ?>public/js/general.js"></script>

     <?php if($this->uri->segment(3)=='articulos' || $this->uri->segment(3)=='consultar') {?> 
        <script src="<?= base_url() ?>public/js/combos.js"></script>
        <script src="<?= base_url()  ?>public/js/articulos.js"></script>
     <?php }?>

     <?php if($this->uri->segment(3)=='categorias')  {?> 
         <script src="<?= base_url()  ?>public/js/categorias.js"></script>
     <?php }?>  

     <?php if($this->uri->segment(3)=='proveedores')  {?> 
         <script src="<?= base_url()  ?>public/js/proveedores.js"></script>
     <?php }?> 



 </body>
 </html>