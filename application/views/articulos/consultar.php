
<body class="skin-green" id='proba2' onload="probar()">

  <!-- Content-Wrapper para el contenedor que tendra el contenido -->
      <div class="content-wrapper">
        
        <!-- contenido -->
        <section class="content">
          <!-- Una fila -->
            <div class="row"">
              <!-- 12 columnas -->
              <div class="col-md-12">
                  <div class="box">
                    <!-- AQUI IRA EL CONTENIDO DEL INDEX -->
                   
                     <div class="box-header bg1">
                       <h3 id="id"> <i class="fa fa-sign-in"></i> Artículo N°  <?php echo $id ?>  </h3>
                    </div>
                    <br>

                   <div class="container">
                       <hr>
                       <div class="card">
                        <h3 class="card-header hcard"><?= $nombre; ?>
                         <span style="font-size: 13px; display: block;"><b style="color: #003C77">Categoria:</b> <?= $nombre_categoria; ?></span>
                        </h3>

                     
                              <div class="card-body">
                                <div class="row">
                                  <div class="col-sm-6">
                                      <table class="table" style="font-size: 15px;">
                                        <tr>
                                          <td style="color: #003C77; font-weight: 10px;">Nombre</td>
                                          <td><?php echo $nombre ?></td>
                                        </tr>
                                        <tr>
                                          <td style="color: #003C77; font-weight: 10px;">Precio</td>
                                          <td><?php echo $precio ?> Bs</td>
                                        </tr>
                                        <tr>
                                          <td style="color: #003C77; font-weight: 10px;">Cantidad Disponible</td>
                                          <td><?php echo $cantidad ?></td>
                                        </tr>
                                         <tr>
                                          <td style="color: #003C77; font-weight: 10px;">Color Disponible</td>
                                          <td><?php echo $color_articulo ?></td>
                                        </tr>
                                        <tr>
                                          <td style="color: #003C77; font-weight: 10px;">Fecha Agregado</td>
                                          <td><?php echo $fecha_agregado ?></td>
                                        </tr>
                                        <tr>
                                          <td style="color: #003C77; font-weight: 10px;">Ubicación</td>
                                          <td><?php echo $anaquel ?></td>
                                        </tr>
                                          <tr>
                                          <td style="color: #003C77; font-weight: 10px;">Descripcion</td>
                                          <td class="text-justify" style="width: 309px;"><?php echo $descripcion ?> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                          tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                          quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                          consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                          cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                          proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </td>
                                        </tr>
                                      </table>

                                 </div>

                                 <div class="col-sm-6">

                                        <table class="table" style="font-size: 15px;">
                                          <tr>
                                            <td style="color: #003C77; font-weight: 10px;">Proveedores</td>
                                            <td> <?php 
                                                    foreach ($proveedores as $key) {
                                                        foreach ($key as $value) {
                                                          echo '<li>'.$value.'</li>';
                                                        }   
                                                    }  
                                                 ?> 
                                                
                                            </td>
                                          </tr>
                                          <tr>
                                            <td colspan="2">
                                                 <button width="30px" height="30px" class="btn btn-info" data-toggle="modal" data-target="#editararticulo"><i class="fa fa-edit"></i> Editar</button>

                                                 <button onclick="eliminar()" class="btn btn-danger"><i class="fa fa-remove" title="Eliminar articulo"></i>  Eliminar</button>

                                                 <button class="btn btn-default bt2" data-toggle="modal" data-target="#cargararticulo" title="Cargar Articulo"><i class="fa fa-cloud-upload colort2"></i> Cargar</button>

                                                 <button class="btn btn-warning" data-toggle="modal" data-target="#descargararticulo" title="Descargar Articulo"><i class="fa fa-cloud-download"></i> Descargar</button>
                                            </td>
                                          </tr>
                                        </table>
                                </div>
                              </div>
                          </div>
      
                         
                              
                           


                           
                         
                     </div>

                        <hr>

                    </div>
                   </div>
                </div>
              </div>
      </section>
    </div>
       


 





<?php require "modal/modalArticuloconsulta.php" ?>

<script type="text/javascript">
  function eliminar(){

    alertify.confirm('Desea borrar', 'Ok', function(){ 
                 var id= <?php echo  $id ?>;

                 $.ajax({
                  type:"POST",
                  url:'http://localhost/InventarioAnvicors/Panel/EliminarArticulo',
                 
                  data:{
                    'datos': id
                  },
                  success:function (data) {
                    alert("eliminado");
                   
                    location.href= 'http://localhost/InventarioAnvicors/Panel/page/articulos';
              

                  },error:function(jqXHR, textStatus, errorThrown){
                    
                    console.log("error");
                  }    


                 });
                
                }
                , function(){

                });
    
  }  
 
    function probar(){

     $.ajax({
            type:"POST",
            url:'http://localhost/InventarioAnvicors/Panel/listarCategoria',
            dataType: "JSON",
            success:function (data) {
             var a = data;
             var arreglo=[];
             var arreglo2=[];
             var indice ;
             var valor;
             var i=0;
          //   console.log(a);
                $.each(a,function(index,item){
                arreglo2=[];
                $.each(item,function(index1,item1){
                  
                  if(index1=="id_categoria"){
                  arreglo2[index1]=item1;

                  }
                  else if(index1=="c_nombre") {
                  arreglo2[index1]=item1;
                  
                 


                  }
                });
                arreglo.push({arreglo2});

              }); 
             //console.log(arreglo);

              
              
            
              $.each(arreglo,function(index,item){
                 $.each(item,function(index1,item1){
              
             $("#seleccion").append('<option value='+item1.id_categoria+'>'+item1.c_nombre+'</option>');
             $("#seleccion").val(<?php echo $categoria ?>);
                });
             
              });
               

            },error:function(jqXHR, textStatus, errorThrown){
              
              console.log("error");
        }

        });
        
           

            $.ajax({
            type:"POST",
            url:'http://localhost/InventarioAnvicors/Panel/listarProveedor',
            dataType: "JSON",
            success:function (data) {
              alert("LLEGO");
             var a = data;
             var arreglo=[];
             var arreglo2=[];
             var indice ;
             var valor;
             var i=0;
           //    console.log(a);
                $.each(a,function(index,item){
                arreglo2=[];
                $.each(item,function(index1,item1){
                  
                  if(index1=="id_proveedor"){
                  arreglo2[index1]=item1;

                  }
                  else if(index1=="p_nombre") {
                  arreglo2[index1]=item1;
                  
                 


                  }
                });
                arreglo.push({arreglo2});

              }); 
              console.log(arreglo);

              
              
            
              $.each(arreglo,function(index,item){
                 $.each(item,function(index1,item1){
              
               $("#seleccionProve").append('<option value='+item1.id_proveedor+'>'+item1.p_nombre+'</option>');
               $("#CargarseleccionProve").append('<option value='+item1.id_proveedor+'>'+item1.p_nombre+'</option>');
                    
                    
                });
             
              });
               

            },error:function(jqXHR, textStatus, errorThrown){
              
              console.log("error");
        }

        });
                 

  
    }
  

 function cargararticulo(){
  var a =[  
  'sadas',
  'sdas'
  ];

  
     var id= <?php echo  $id ?>;
     var cantidadactual =<?php echo  $cantidad ?>;
     var inputcantidadcarga= $('#inputcantidadcarga').val();
     var valor2= parseInt($('#inputcantidadcarga').val());
     var seleccionProve  =$("#CargarseleccionProve").val();
     var t=parseInt(cantidadactual);
     var  total=valor2+t;

     if(seleccionProve == 'Seleccione'){
      seleccionProve=null;     
    }

     if(valor2<0){
      inputcantidadcarga=null;
     }
     
     

     $.ajax({
       type:"POST",
       url:'http://localhost/InventarioAnvicors/Panel/Cargararticulo',
       dataType: "HTML",
       data:{
        'id_articulo':id,
        'inputcantidadcarga':inputcantidadcarga,
        'seleccionProve':seleccionProve,
        'total':total

    
       },
      success:function (data){
      //  if(data!=true){  
        console.log(data);
        //}
        //else{
        if(data!=true){ 
        alertify.alert("<b style='color: red;'>IMPORTANTE</b>", "Debes llenar los siguientes campos: <br>"+data, function(){alertify.message('Ok'); });
        }
        else {
        location.href= url+'Panel/consultar?value='+id+'';
        }
        //} 
        

       }
    


     });
   

    }



    function descargararticulo(){
      var a = parseInt($('#cantidadDescarga').val());
      alert(a);
      var e = <?php echo $cantidad ?>;
      var id = <?php echo $id ?>;
      var r = parseInt(e);

       if ( isNaN(a) == true ){
      alertify.alert("<b style='color: red;'>IMPORTANTE</b>", "Solo Numeros Verifique <br>", function(){alertify.message('Ok'); });

      }
      else if(a==''){
         alertify.alert("<b style='color: red;'>IMPORTANTE</b>", "Debe llenar el campo <br>", function(){alertify.message('Ok'); });


      }
      else if (a<0){
         alertify.alert("<b style='color: red;'>IMPORTANTE</b>", "El numero no puede ser negativo<br>", function(){alertify.message('Ok'); });
      }
      else if (a>r){
          alertify.alert("<b style='color: red;'>IMPORTANTE</b>", "No puede descargar mas articulos de los ya existentes <br>", function(){alertify.message('Ok'); });

      }
      
      else {
        var resultado = r-a;
        $.ajax({
       type:"POST",
       url:'http://localhost/InventarioAnvicors/Panel/descargarArticulo',
       
       data:{
        'cantidad':resultado,
        'id':id
       },
      success:function (data){
        if(data==true){


       location.href= url+'Panel/consultar?value='+id+'';
       }
        else{
          alert('error amiwo :C ' );
        }
      
       }
     });



      }





    }          

</script>