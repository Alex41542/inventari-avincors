    <!-- MODAL PARA REGISTRAR UN NUEVO ARTICULO -->

    <div class="modal fade" id="nuevoarticulo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">

          <div class="modal-header bg-header-modal">
            <h4 class="modal-title" id="myModalLabel"><i class="fa fa-edit"></i> Nuevo Articulo</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" onclick="limpiar()">&times;</span></button> 
          </div>

          <div class="modal-body bg-body-modal">
            <form id="form_articulo">
              <div class="row">
                <div class="col-md-6"> 
                   <b><label>Nombre</label></b>
                    <input type="text" id="a_nombre" name="a_nombre"placeholder="Marca y Modelo" class="form-control input-sm limpiar"/>

                    <b><label for="a_cantidad">Cantidad</label></b>
                    <input type="number" id="a_cantidad" name="a_cantidad" placeholder="Cantidad" class="form-control input-sm limpiar"/>
                </div>
                <div class="col-md-6"> 
                   <b> <label for="a_descripcion">Descripcion</label></b>
                    <textarea id="a_descripcion" name="a_descripcion" placeholder="Se describe el artículo; mediciones, especificaciones y demás carácteristicas" class="form-control input-sm limpiar" rows="4" cols="50"></textarea>
                </div>
              </div>
              <div class="row">
                  <div class="col-md-6">
                       <b><label for="Precio">Precio para venta</label></b>
                       <input id="a_precio_venta" name="a_precio_venta" type="text" name="Precio" placeholder="Precio" class="form-control input-sm limpiar"/>
                  </div>
                  <div class="col-md-6">
                        <div class="div-select">
                            <b><label>Categoria: </label></b>
                             <select id="cbocategoria" name="cbocategoria" class="form-control">
                                <option value="" selected="selected">Seleccionar</option>
                             </select>
                         </div> 

                  </div>
              </div>

              <div class="row">

                  <div class="col-md-6">    
                      <div class="div-select">
                            <b><label>Proveedor: </label></b>
                           <select id="cboproveedor" name="cboproveedor" class="form-control">
                              <option  value="" >Seleccionar</option>
                           </select>
                      </div>
                  </div>

                  <div class="col-md-6">
                     <div class="div-select">
                          <b><label>Color: </label></b>
                         <select id="cbocolores" name="cbocolores" class="form-control">
                            <option  value="" selected="selected">Seleccionar</option>
                         </select>
                     </div>
                  </div>

              </div>
               
                <b><label>Ubicación de Anaquel</label></b>
                <div class="row">
                      
                        <div class="col-md-4">
                           <select id="cboanaquel1" class="form-control">
                              <option value="">Seleccionar</option>
                           </select>
                        </div>
                        
                        <div class="col-md-4">
                           <select id="cboanaquel2" class="form-control">
                              <option class="sele1" value="">Seleccionar</option>
                           </select>
                        </div>

                        <div class="col-md-4">
                           <select id="cboanaquel3" name="cboanaquel3" class="form-control">
                              <option class="sele2"  value=" " selected="selected">Seleccionar</option>
                           </select>
                        </div>
                         
                     
                  </div>  
            </form>
          </div>

          <div class="modal-footer bg-footer-modal">
            <button type="button" class="btn btcerrar" data-dismiss="modal" onclick="limpiar()">Cerrar</button>
            <button class="btn btcrear" onclick="createarticulo()">Crear</button>
          </div>
        </div>
      </div>
    </div>
         
<!--  FIN MODAL PARA REGISTRAR UN NUEVO ARTICULO -->