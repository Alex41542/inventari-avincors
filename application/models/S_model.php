
<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class S_model extends CI_Model {

  public function __construct(){
		parent:: __construct();
   // $this->load->database();
	}

  public function verificar_sesion($usuario, $clave){
      $consulta = $this->db->get_where('usuarios', array('usuario' => $usuario,
                                                         'clave'   => $clave ));

      if($consulta->num_rows() == 1){
        return $consulta->row();
      }
      else{
        return false;
      }
  }

    public function updateUsuario($data, $data2){

     $this->db->where('id_usuario', 1);
     $this->db->update('usuarios',array('usuario' => $data,
                                        'clave' => $data2));
     $this->db->select('*');
     $consulta = $this->db->get('usuarios');
      return $consulta->row();
  }  

  public function insertCategoria($data){
    
    $this->db->insert('categorias', $data);
    return $insert_id = $this->db->insert_id();

  }  

  public function updateCategoria($id_categoria, $data){
    
     $this->db->update('categorias', $data, array('id_categoria' => $id_categoria));
     return $this->db->affected_rows();

  }

   public function updateProveedor($id_proveedor, $data){
     $this->db->where('id_proveedor', $id_proveedor);
     $this->db->update('proveedores', $data);
   
    return true;
    

  }

  public function getByIdCategoria($id_categoria){
    $this->db->from("categorias");
    $this->db->where('id_categoria' , $id_categoria);
    $query = $this->db->get();

    return $query->row();
  }  

  public function getByIdProveedor($id_proveedor){
    $this->db->from("proveedores");
    $this->db->where('id_proveedor' , $id_proveedor);
    $query = $this->db->get();

    return $query->row();
  }  

  public function insertProveedor($data){
    
    $this->db->insert('proveedores', $data);
    $this->db->insert_id();

    if ($this->db->affected_rows() > 0) {
      return true;
    }
    else{
      return false;
    }
  }

  public function buscarArticuloProveedor($id_proveedor){
            $this->db->select('a_nombre');
       $s = $this->db->get_where('v_listar_articulo', $id_proveedor);
       return $s->result();
  }

  public function getCat(){
    $this->db->select('id_categoria, c_nombre, c_descripcion, c_fecha_agregado, estado');
    $this->db->where('estado', true);
    $this->db->from('categorias');

    $r = $this->db->get();

    return $r->result();
  } 

  public function getProv(){
    $this->db->select('id_proveedor, p_nombre, p_telefono_cel, p_telefono_ofi, p_correo, estado');
    $this->db->where('estado', true);
    $this->db->from('proveedores');

    $r = $this->db->get();

    return $r->result();
  }

  public function selectCategoria($estado){
    $s = $this->db->get_where('categorias', $estado);
    return $s->result();
  }

  public function selectColores(){

    $s = $this->db->get('colores');
    return $s->result();
  }

  public function selectProveedor($estado){
    $s = $this->db->get_where('proveedores', $estado);
    return $s->result();
  }

  public function selectNroAnaquel(){
    $this->db->select('*');
    $this->db->from('anaquel');
    $data = $this->db->get();

    return $data->result_array(); 
  }

  public function selectSecAnaquel($sec){
    $s = $this->db->get_where('anaquel', $sec);
    return $s->result();
  }

  public function selectColAnaquel(){
     $this->db->select('*');
     $this->db->from('anaquel');
     $data = $this->db->get();

     return $data->result_array(); 
   
  }

  public function dropProveedor($id_proveedor){
     $a = array('estado' => FALSE);
     $this->db->where('id_proveedor', $id_proveedor);
     $this->db->update('proveedores', $a);
     return TRUE;
  }  

  public function dropCategoria($id_categoria){
     $a = array('estado' => FALSE);
     $this->db->where('id_categoria', $id_categoria);
     $this->db->update('categorias', $a);
     return TRUE;
  }

  public function getArticulos(){
   // $this->db->select('idcategoria, cnombre, cdescripcion');
    $this->db->select('*');
    $this->db->from('v_articulos_simple');

    $data = $this->db->get();

    return $data->result_array(); 
  }


  public function insertArticulo($data_articulos){
    $this->db->insert('articulos', $data_articulos);
    return $insert_id = $this->db->insert_id();
  }

  public function insertRelationArticulos($data_prov_art, $data_color_art){
    $this->db->insert('lista_prov_articulo', $data_prov_art);
    $this->db->insert('articulos_colores', $data_color_art);
    return $id = $this->db->insert_id();
  }

  public function obtenerarticulo($a){
    $this->db->select('*');
    $this->db->from('v_listar_articulo');
    $this->db->where('id_articulo',$a);
    $this->db->limit(1);
    $data = $this->db->get();
    return $data->result_array(); 
    

  }

  public function obtenerProveedor($a){
    $this->db->select('p_nombre');
    $this->db->from('v_listar_articulo');
    $this->db->where('id_articulo',$a);
    $data = $this->db->get();
    return $data->result_array(); 
  }

  public function EditarArticulo($a){
    
    $this->db->where('id_articulo',$a["id_articulo"]);
    $this->db->update("articulos",$a);



  }

  public function dropArticulo($id_articulo){

    $this->db->where('id_articulo', $id_articulo);
    $this->db->delete("articulos");
    
    return true;
     

  }

  public function rowCount($tabla){

    if($tabla != 'articulos'){
       $this->db->select('*');
       $this->db->where ('estado', TRUE);
       $this->db->from($tabla);
       $result = $this->db->get();
       // print_r($result->num_rows());
      return $result->num_rows();

    }
    else{
      $this->db->select('*');
      $this->db->from($tabla);
      $result = $this->db->get();
      return $result->num_rows();
    }


  }


   public function cargararticulo($arreglo){
   
    $this->db->where('id_proveedor',$arreglo['idprovedor'])->
    where('id_articulo',$arreglo['idarticulo']);
    $this->db->from('lista_prov_articulo');
    $a=$this->db->get();
    if($a->num_rows() > 0){
    $data =array(
      'a_cantidad'=> $arreglo['total']
    );
    $this->db->where('id_articulo',$arreglo['idarticulo']);
    $this->db->update('articulos',$data);
    return true;
    }
    else if($a->num_rows()==0) {
      $arreglo2=array(
      'id_proveedor'=>$arreglo['idprovedor'],
      'id_articulo'=>$arreglo['idarticulo']
      );
      $this->db->insert('lista_prov_articulo',$arreglo2);
       $data =array(
      'a_cantidad'=> $arreglo['total']
    );
    $this->db->where('id_articulo',$arreglo['idarticulo']);
    $this->db->update('articulos',$data);
     return true;

    }

    else{
      return false;
    }





  }

  public function descargarArticulo($a){
    $data = array(
    'a_cantidad'=>$a['a_cantidad']
    );
    $this->db->from('articulos');
    $this->db->where('id_articulo',$a['id_articulo']);
    $a1=$this->db->get();
    if($a1->num_rows() > 0){


    $this->db->update('articulos',$data);
    return true;
  }else {
    return false;
  }
  



  }




}