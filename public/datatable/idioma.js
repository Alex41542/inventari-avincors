var idioma_espanol = {
    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_",
    "sZeroRecords":    "No se encontraron registros",
    "sEmptyTable":     "No existen datos en esta tabla",
    "sInfo":           "Registros del <b> _START_ al _END_ </b> | <b style='color:teal;'> _TOTAL_ registros totales </b> |",
    "sInfoEmpty":      "Registros 0 / 0 | 0 registros totales |",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
}