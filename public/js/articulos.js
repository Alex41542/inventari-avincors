function cargartbArticulo(){

          var url;
          var recibe;
        $.ajax({
            url: "http://localhost/InventarioAnvicors/Panel/cargarArticulosInicial",
            type: 'POST',
            dataType: 'JSON',
          success: function(data){
                   console.log(data);
                   var a = data;
                  $("#tb_articulo").DataTable({

                      "autoWidth": false,
                      "destroy":true,
                      "dataType":"JSON",
                      "Type":"POST",
                      "language": idioma_espanol,
                      "order":[[ 0,'desc' ]] ,
                      "defaultContent":"<i>no</i>",
                      "dom":  'lBf<t>pi',
                      
                 buttons: [
             //   { extend: 'pdfHtml5'}
                  // {className:'botoninvisible' },
                //{className:'botoninvisible' },
                {extend: 'pdf',  className:'botonesd btn btn-sm btn-danger',titleAttr:'Obtener Reporte en pdpf',text:'<i class="fa fa-file-pdf-o" style="color: white;"></i> Reporte' ,
                   init: function(api, node, config) {
                   $(node).removeClass('dt-button');
                   } 
                
                } ,
                {extend: 'excel',  className:'botonesd btn btn-sm btn-success',titleAttr:'Obtener Reporte en excel',text:'<i class="fa fa-file-excel-o" style="color: white;"></i> Reporte' ,
                   init: function(api, node, config) {
                   $(node).removeClass('dt-button');
                   } 
                
                } 
                
                
                ],
                     "columns":[
                        {'data':"id_articulo", 'sClass':'hidden'},
                        {'data':"a_nombre"},
                        {'data':"c_nombre"},
                        {'data':"a_cantidad"},
                        {"orderable": true,
                          render: function(data, type, row){
                              return row.a_precio_venta+' Bs';
                          }

                        }

                      ],
                      "columnDefs":[
                         {
                        "data":null,
                        "defaultContent": "<button  title='Eliminar' onclick='tbarticulodrop(this)' class='btn btn'> <i class='fa fa-trash'></i> </button>&nbsp <button title='Consultar' onclick='tbarticuloConsulta(this)'  class='btn  btn-primary btn-outline-light'>&nbsp<i class='fa fa-edit'></i></button>",
                        "targets":[5]
                         }, 

                             ]
                     //"dom":  '<<t>lip>',
                
              });

              var styles = {
                position:'relative',
                float:'right',
                right:'-20px'
               // float:'right',

             };

               $(".dt-buttons").find($(".botoninvisible")).css({'visibility':'hidden'});
              //$(".dt-buttons").find($(".botonesd")).css({'maring-left'});
                   
               $.each(a,function(index,item){
                $("#tb_articulo").DataTable().row.add(item).draw();  
               });
          }

     });
            
           
         
    }



    function tbarticulodrop(botonr=null){
      swal({
              title: "¿Estás seguro de eliminar este articulo?",
              text: "Al eliminarlo, ya no podra volver a restaurarlo",
              type: "warning",
              showCancelButton: true,
              confirmButtonClass: "btn-danger",
                  confirmButtonText: "Eliminar",
             closeOnConfirm: false
          },

          function(isConfirm){
           if (isConfirm){
          
                 var arreglo = [];

                $(botonr).parent("td").parent("tr").find("td").each(function(index, item){
                  var valor = $(this).text().trim();
                  arreglo.push(valor);
                });

                var id_articulo;
                $(arreglo).each(function(index, item){
                  // alert(index+":"+item);
                  if(index=="0"){
                    id_articulo = item;
                  }

                });

                $.ajax({
                            url:url+"Panel/eliminarArticulo",
                            type:"POST",
                            data:{'id_articulo': id_articulo },
                            datatype: "JSON",
                            success:function (respuesta) {
                            swal("Articulo eliminado correctamente", "Presiona el boton para continuar", "success");
                            var table = $('#tb_articulo').DataTable();
           
                              table.clear().draw();
                              table.destroy();
                              cargartbArticulo();
                              
                            },
                            error:function(jqXHR, textStatus, errorThrown){
                          alert(jqXHR);
                        }

                      });
            }else{
              alertify.error("Cancelado");
            }
          
        });
    	



	}






    //Esta funcion toma los valores del registro apra enviarlos a consulta

    function tbarticuloConsulta(botonr=null){
      var arreglo =[];
     
          
          $(botonr).parent("td").parent("tr").find("td").each(function (index , item) {

            var valor = $(this).text().trim();
            arreglo.push(valor);
          });
          var a={};
          var inidice;
          $(arreglo).each(function (index , item){
           // alert(index+":"+item);
          if(index=="0"){
              inidice=item;
          }
          a["n"+index+""]=item;
          // alert(a);
          

          });
        //   $.ajax({
         //   url: link+"Panel/vistaconsulta",
          //    type: 'POST',
              
           //   data:{ 'arreglo':a},
             
           //   error:function(xhr, status){
           //     console.log(xhr+""+ status);
           //   },
           //   success:function(data){
              //   var resultado;
              //   $.each(data,function(index,item){
                 //   if(index=='n0'){
                 //    resultado=item;
                 //   }
                 //   console.log(resultado);
                   $("#body").remove();
                   location.href= url+'Panel/consultar?value='+inidice+'';
          
                // });
              
             
         //      
                // $('#prueba').empty().html(data);

            //  }
            

          // }); 
    }

       /*
  function agregarArticulo(){
  var table = $("#tb_articulo").DataTable();
    var nombre = $("#nombreagregar").val();
    var precio = $("#precioagregar").val();

    var array={}

    array['nombre']=nombre;
    array['precio']=precio;

    table.clear();
    table.row.add(array).draw(); 
 
      $("#nombreagregar").val("");
    $("#precioagregar").val("");
    $("#descripcionagregar").val("");
    $("#detalleagregar").val("");
    $("#agregarcategoria").val(0);
    $("#agregarproovedor").val(0);
    $("#precioagregar").val("");
    $("#cantidadagregars").val("");


   
    



  }

*/

/*

$("#a_precio_venta").on({
    "focus": function (event) {
        $(event.target).select();
    },
    "keyup": function (event) {
        $(event.target).val(function (index, value ) {
            return value.replace(/\D/g, "")
                        .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".");
        });
    }
});

*/

function add_articulo(){

            $('#form_articulo')[0].reset(); 
            $('#nuevoarticulo').modal('show');
            $('.modal-title').html('<i class="fa fa-edit""></i> Nuevo Articulo'); 
      }

function createarticulo(){
    var a_nombre = $('#a_nombre').val();
    var a_descripcion = $('#a_descripcion').val();
    var id_categoria = $('#cbocategoria').val();
    var id_proveedor = $('#cboproveedor').val();
    var id_color = $('#cbocolores').val();
    var a_precio_venta = $('#a_precio_venta').val();
    var a_cantidad = $('#a_cantidad').val();
    var id_anaquel = $('#cboanaquel3').val();

    $.ajax({
                  url:url+"Panel/agregarArticulo",
                  type:"POST",
                  data:{
                    'a_nombre':     a_nombre,
                    'a_descripcion':  a_descripcion,
                    'id_categoria':  id_categoria,
                    'id_proveedor':  id_proveedor,
                    'id_color':  id_color,
                    'a_precio_venta':  a_precio_venta,
                    'a_cantidad':  a_cantidad,
                    'id_anaquel':  id_anaquel
                  },
                  success:function (respuesta) {
                      if(respuesta == true){
                         $(".modal").modal('hide');
                          swal("Articulo agregado correctamente", "Presiona el boton para continuar", "success");
                          $('#form_articulo')[0].reset(); 
                          var table = $('#tb_articulo').DataTable();
         
                            table.clear().draw();
                            table.destroy();
                            cargartbArticulo();
                       }
                       else{
                         alertify.alert("<b style='color: red;'>IMPORTANTE</b>", "Debes llenar los siguientes campos: <br>"+respuesta, function(){alertify.message('Ok'); });
                       }

                    },
                  error:function(jqXHR, textStatus, errorThrown){
                    alertify.success('Agregado con errores');
                      var table = $('#tb_articulo').DataTable();
     
                        table.clear().draw();
                        table.destroy();
                       cargartbArticulo();
              }

            });
}

function lod(){

  alert('asdasdsa');
}




