
		
	function loadtable(){


		 $('#tb_categoria').DataTable({
			"paging": true,
			"info": true,
			"filter": true,
			"order": [[ 3, 'desc' ]],
			"language": idioma_espanol,


			"ajax": {
				"url": url+"Panel/listarCategoria",
				"type": "POST",
				dataSrc: ''
			},
			"columns": [
				{data: "c_nombre"},
				{data: "c_descripcion", 'sClass': 'id'+"id_categoria"},
				{data: "c_fecha_agregado"},
				{"orderable": true,
					render: function(data, type, row){
							return '<button title="Eliminar" onclick="eliminar('+row.id_categoria+')" class="btn btn-danger"><i class="fa fa-trash"></i></button> '+
							' <button title="Editar" onclick="editar('+row.id_categoria+')" class="btn btn-warning"><i class="fa fa-edit"></i></button>';
							
					}

				}


			]	
			
			 });						   
	}									     	
			function add_categoria(){
				 save_method = 'add';
			      $('#form')[0].reset(); 
			      $('#modal_categoria').modal('show');
			      $('.modal-title').html('<i class="fa fa-folder""></i> Nueva Categoria'); 
			}

	

						
			
		
		function eliminar($id){
					var id_categoria = $id;
					swal({
						  title: "¿Estás seguro de eliminar esta categoria?",
						  text: "Al eliminarla, ya no podra volver a restaurarla",
						  type: "warning",
						  showCancelButton: true,
						  confirmButtonClass: "btn-danger",
								  confirmButtonText: "Eliminar",
					 	 closeOnConfirm: false
					},

				function(isConfirm){
					 if (isConfirm){
		    	
							$.ajax({
									        url:url+"Panel/eliminarCategoria",
									        type:"POST",
									        data:{'id_categoria': id_categoria },
									        datatype: "JSON",
									        success:function (respuesta) {
												swal("Categoria eliminada correctamente", "Presiona el boton para continuar", "success");
													var table = $('#tb_categoria').DataTable();
				 
													 	table.clear().draw();
														table.destroy();
														loadtable();
									        	
								       	 	},
								       	 	error:function(jqXHR, textStatus, errorThrown){
												alert(jqXHR);
											}

							    	});

		  			}else{
		  				alertify.error("Cancelado");
		  			}
				  
				});

		
			
		}


		function editar(id){
			save_method = 'update';
			$('#form')[0].reset(); 
			

			 $.ajax({
		        url : url+"panel/dataCategoria/"+id,
		        type: "GET",
		        dataType: "JSON",
		        success: function(data)
		        {
		         
		          $('[name="id_categoria"]').val(data.id_categoria);
		          $('[name="c_nombre"]').val(data.c_nombre);
		          $('[name="c_descripcion"]').val(data.c_descripcion);
		          
		          
		          $('#modal_categoria').modal('show');
			      $('.modal-title').html('<i class="fa fa-edit""></i> Editar Categoria'); 
		            
		          },
		          error: function (jqXHR, textStatus, errorThrown)
		          {
		            alert('Error de data');
		          }
		     });
		}

	







		function save(){

			var urlMethod;

			var attr = document.querySelector("#createcategoria"); 
			attr.removeAttribute("data-dismiss");

			var c_nombre = $('#c_nombre').val();
			var c_descripcion = $('#c_descripcion').val();
				
				if(c_nombre == null || c_nombre.length == 0 || /^\s+$/.test(c_nombre) ){
					$('#spanError1').show();
				
					if(c_descripcion == null || c_descripcion.length == 0 || /^\s+$/.test(c_descripcion) ){
							$('#spanError2').show();
					}
				}

				else if(c_descripcion == null || c_descripcion.length == 0 || /^\s+$/.test(c_descripcion) ){
							$('#spanError2').show();
				}
				
				else{


				      if(save_method == 'add'){
				        urlMethod		 = url+'panel/agregarCategoria';
				        var id_categoria = '';
				      }
				      else{
				        var id_categoria = $('#id_categoria').val();
				        urlMethod =  url+'panel/actualizarCategoria/';
				      }

					attr.setAttribute("data-dismiss", "modal");
					
					$.ajax({
					        url: urlMethod,
					        type:"POST",
					        data:{
					        	'c_nombre': 		c_nombre,
					        	'c_descripcion': 	c_descripcion,
					        	'id_categoria' : 	id_categoria
					        },
					        datatype: "JSON",
					        success:function (data) {
					        	if(data.status){
									swal("Su categoria fue registrada exitosamente", "Presione el boton para continuar", "success")
									$('.limpiar').val('');
									var table = $('#tb_categoria').DataTable();
 
									 	table.clear().draw();
										table.destroy();
										loadtable();
					        	}
				       	 	},
				       	 	error:function(jqXHR, textStatus, errorThrown){
								alert("Ocurrio un fallo");
								alertify.success('Agregado con errores');
									var table = $('#tb_categoria').DataTable();
 
									 	table.clear().draw();
										table.destroy();
										loadtable();
							}

			    	});

				}

		} //FINAL FUNCION DE AGREGAR CATEGORIA

		$('#c_nombre').click(function(){
			$('#spanError1').hide();
		});

		$('#c_descripcion').click(function(){
			$('#spanError2').hide();
		});



		