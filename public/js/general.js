

			function limpiar(){
                $('.limpiar').val('');
                $('#spanError1').hide();
                $('#spanError2').hide();
                 alertify.error('Cerrado');
            }

            function dialog(){
            	
		           alertify.genericDialog || alertify.dialog('genericDialog',function(){
					    return {
					        main:function(content){
					            this.setContent(content);
					        },
					        setup:function(){
					            return {
					                focus:{
					                    element:function(){
					                        return this.elements.body.querySelector(this.get('selector'));
					                    },
					                    select:true
					                },
					                options:{
					                    basic:true,
					                    maximizable:false,
					                    resizable:false,
					                    padding:false
					                }
					            };
					        },
					        settings:{
					            selector:undefined
					        }
					    };
					});
					
					alertify.genericDialog ($('#loginForm')[0]).set('selector', 'input[type="password"]');
            }

            function guardado(){
            	alertify.message('Usuario actualizado')
            }

     