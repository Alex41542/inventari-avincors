		
		
	function loadtable(){	 
		 $('#tb_proveedor').DataTable({
			"paging": true,
			"info": true,
			"filter": true,
			"order": [[ 0, 'desc' ]],
			"language": idioma_espanol,


			"ajax": {
				"url": url+"Panel/listarProveedor",
				"type": "POST",
				dataSrc: ''
			},
			"columns": [
				{data: "id_proveedor", 'sClass':'hidden'},
				{data: "p_nombre"},
				{data: "p_telefono_cel"},
				{data: "p_telefono_ofi"},
				{data: "p_correo"},
				{"orderable": true,
					render: function(data, type, row){
							return '<button class="btn btn-success" onclick="lista('+row.id_proveedor+')"">Lista</button>';
					}

				},
				{"orderable": true,
					render: function(data, type, row){
							return '<button title="Eliminar" onclick="drop('+row.id_proveedor+')" class="btn btn-danger"><i class="fa fa-trash"></i></button> '+
							'<button title="Editar" onclick="editar('+row.id_proveedor+')" class="btn btn-warning"><i class="fa fa-edit"></i></button>';
					}

				},
			],

			"columnDefs":[
						{
                        "targets":[2,3,4],
                        "createdCell":function(td, cellData, rowData, row, col){
                           if( cellData == "" || cellData == 0) {
                           $(td).html("<b style='color:teal'>No aplica</b>"); 
                           }
                         }
                        }
						 ]


										   
										     	
			});	 
	}

		// $.post(url+"Panel/listarProveedor",
		// 	          {"estado": 1}, 

		// 	        function (data){
		// 	          var proveedor = data;
		// 	          $.each(proveedor, function(i, item){
			       			
		// 	       			var a = item.p_telefono_cel;
		// 	       			var b = item.p_telefono_ofi;
		// 	       			var c = item.p_correo;

		// 	          		if(item.p_telefono_cel == '0'){
		// 	          			var a = 'No aplica';
		// 	          		}
		// 	          		if(item.p_telefono_ofi == '0'){
		// 	          			var b = 'No aplica';
		// 	          		}
		// 	          		if(item.p_correo == ''){
		// 	          			var c = 'No aplica';
		// 	          		}

		// 	          		$('#tbody').append('<tr><td>'+item.p_nombre+'</td>'+
		// 	          									'<td>'+a+'</td>'+
		// 	          									'<td>'+b+'</td>'+
		// 	          									'<td>'+c+'</td>'+
		// 	          									'<td><button class="btn btn-success" onclick="lista('+item.id_proveedor+')"">Lista</button></td>'+
		// 	          									'<td><button title="Eliminar" onclick="drop('+item.id_proveedor+')" class="btn btn-danger"><i class="fa fa-trash"></i></button></td></tr>'
		// 	          									);

			          	
			          	
		// 	          });
							 
		// 	  	   });
			
		function add_proveedor(){
			 	 save_method = 'add';
			      $('#form_proveedor')[0].reset(); 
			      $('#modal_proveedor').modal('show');
			      $('.modal-title').html('<i class="fa fa-truck""></i> Nuevo Proveedor'); 
			}


		function drop($id){
			var id_proveedor = $id;
			$.ajax({
					        url:url+"Panel/eliminarProveedor",
					        type:"POST",
					        data:{'id_proveedor': id_proveedor},
					        datatype: "JSON",
					        success:function (respuesta) {
									alertify.error(respuesta);
									var table = $('#tb_proveedor').DataTable();
 
									 	table.clear().draw();
										table.destroy();
										loadtable();
					        	
				       	 	},
				       	 	error:function(jqXHR, textStatus, errorThrown){
								alert(jqXHR);
							}

			    	});
	
		}


		function editar(id){
		
			
			
			save_method = 'update';
			$('#form_proveedor')[0].reset(); 
			

			 $.ajax({
		        url : url+"panel/dataProveedor/"+id,
		        type: "GET",
		        dataType: "JSON",
		        success: function(data)
		        {

		         if(data.p_telefono_ofi == '0' || data.p_telefono_ofi==0){
                  data.p_telefono_ofi =null;
		         }
		         if (data.p_telefono_cel == '0' || data.p_telefono_cel==0){
                 data.p_telefono_cel=null;
		         }
 		          $('[name="id_proveedor"]').val(data.id_proveedor);
		          $('[name="p_nombre"]').val(data.p_nombre);
		          $('[name="p_numero1"]').val(data.p_telefono_cel);
		          $('[name="p_numero2"]').val(data.p_telefono_ofi);
		          $('[name="p_correo"]').val(data.p_correo);
		    //     console.log( $('[name="p_nombre"]').val(data.p_nombre));
		          
		          $('#modal_proveedor').modal('show');
			      $('.modal-title').html('<i class="fa fa-edit""></i> Editar Proveedor'); 
		            
		          },
		          error: function (jqXHR, textStatus, errorThrown)
		          {
		            alert('Error de data');
		          }
		     });
		     
		}


		function lista($id){
			console.log('------');
			 alertify.alert("Lista de artículos",
			 	$.post(url+"Panel/listarArticuloProveedor",{"id_proveedor": $id},

					function (data){
						 var proveedor = JSON.parse(data);

						 if(proveedor == ''){

						 	$('.ajs-content').append('<span class="para_remover">No existen articulos para este proveedor</span>');

						 }
						 else{
							 $.each(proveedor, function(i, item){
							 	$('.ajs-content').append('<li class="para_remover">'+item.a_nombre+'</li>');
					          });
						 }
	                }),
			 		function(){
			 			$('.para_remover').remove();
			  			alertify.message('Ok');

			  		});
		}








		function save(){

			var urlMethod;

			var attr = document.querySelector("#createproveedor"); 
		//	attr.removeAttribute("data-dismiss");

			var p_nombre = $("input[name='p_nombre']").val();
			var p_telefono_cel = $("input[name='p_numero1']").val();
			var p_telefono_ofi = $("input[name='p_numero2']").val();
			var p_correo = $("input[name='p_correo']").val();
			var id_proveedor= $("input[name='id_proveedor']").val();
				

					 if(save_method == 'add'){
				        urlMethod		 = url+'panel/agregarProveedor';
				        var id_proveedor = '';
				      }
				      else{
				        var id_categoria = $('#id_proveedor').val();
				        urlMethod =  url+'panel/actualizarProveedor/';
				      }

                     
                    var a = 0 ;
                 
                   
					$.ajax({

					        url: urlMethod,
					        type:"POST",
					        data:{
					        	'p_nombre': 	p_nombre,
					        	'p_telefono_cel': 	p_telefono_cel,
					        	'p_telefono_ofi': 	p_telefono_ofi,
					        	'p_correo': 	p_correo,
					        	'id_proveedor': id_proveedor
					        },
					        success:function (respuesta){ 
					        	a=a+1;
					        	if (respuesta == true) {	
					        	    
									
									swal("Proveedor agregado correctamente", "Presiona el boton para continuar", "success");
									$('.limpiar').val('');
								    $(".modal").modal('hide');

									var table = $('#tb_proveedor').DataTable();
                                    
									 	table.clear().draw();
										table.destroy();
										loadtable();
								}		
								else{

									$("#spanError2").show();
									$(".errors").html(respuesta);
								}
									     
								
	
				       	 	},
				       	 	error:function(jqXHR, textStatus, errorThrown){
								alert("marico el que lo lea");
							}

			    	});

                 cerrartm();
			    	
					
				      
				
		}



		$('#p_nombre').click(function(){
			$('#spanError2').hide();
		});

		$('#p_correo').click(function(){
			$('#spanError2').hide();
		});
